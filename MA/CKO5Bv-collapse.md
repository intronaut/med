ZSO Collaps 

Literatuur te bestuderen voorafgaand aan ZSO:
    • Leerboek acute geneeskunde (hoofdstuk 7)
    • Qportaal (Radboudumc): Advanced life support bij volwassenen
    • Eventueel: leerboek Interne Geneeskunde 
    • Syncope clinical management in the emergency department: a consensus from the first international workshop on syncope risk stratification in the emergency department Costantino, et al. Eur Heart J 2016 37: 1493-1498

Inleiding
Een collaps, syncope of wegraking is een breed begrip. De differentiaaldiagnose hierbij is erg breed. Een goede differentiaaldiagnose kan je helpen met het klinisch redeneren in het vervolg van de onderstaande casus. 

### Vraag 1  Maak voor je zelf een uitgebreide differentiaaldiagnose van een collaps.

- vasovagaal (vasodilatatie ; hyperthermie ; dehydratie)
- cardiaal: Afib, bundeltakblok
- hypoperfusie in cerebro:  stenose carotis? Stuttering CVA?
- psychogeen / psychostress
- epilepsie
- trauma capitis
- hypoglycaemisch
- kataplexie
- intox


Casus 1
Een 24-jarige patiënte wordt in de middag binnengebracht op de SEH. Haar vriendinnen vertellen dat patiënte aan het winkelen was en plotseling niet lekker werd, hierbij is zij kort buiten bewustzijn geweest. 

### Vraag 2 Welke informatie wil je weten van de ambulance? 

- wat ging er aan vooraf?
- VG?
- indruk pte

Vervolg casus
Patiënte wordt aangesloten op de monitor en je doet de primary survey:
    • A: vrij (patiënte praat, heeft geen losse elementen in de mond), CWK niet drukpijnlijk
    • B: Vesiculair ademgeruis beiderzijds, symmetrische thoraxexcursies, SaO2 97%, Ademhalingsfrequentie 18/min
    • C: Iets bleek, niet klam, capillaire refill < 3 sec, abdomen soepel, bekken en lange pijpbeenderen stabiel. 
    • D: EMV maximaal, patiënte reageert iets traag, pupillen lichtreactief, geen tekenen van lateralisatie
    • E: Temp 36.8, glucose 5.8

Vraag 3 Stel een korte differentiaaldiagnose op naar aanleiding van bovenstaande gegevens. 

- vasovagale collaps ; reactieve syncope

Vraag 4 Zou je aanvullend onderzoek inzetten aan de hand van je differentiaaldiagnose? Zo ja, welk onderzoek?

-  

### Vraag 5 Aangezien patiënte stabiel is heb je nu tijd om een goede anamnese en hetero-anamnese af te nemen. Wat wil je patiënte vragen? 

- aanleiding naar eigen inzicht?
- eerder in VG?
- vaker near-collaps / sterretjes voor de ogen (orthostatische hypotensie)
- bloeddruk gemeten ooit?
- psychiatrisch VG?
- bloedarmoede?
- menstruatie
- schokkken/trillingen/bevingen → hoe vaak, hoe veel, symmetrisch,


<!-- Vervolg casus
Patiënte geeft aan dat ze een lange dag heeft gehad en weinig heeft gegeten. Ze voelde zich eigenlijk de hele dag al slap. Toen ze lang moest wachten in een drukke rij voelde ze zich licht in het hoofd worden en viel op de grond. Haar vriendinnen voegen toe dat het er heel akelig uit zag. Ze kregen geen goed contact met hun vriendin, ze begon zelfs met de armen te trekken en strekken. Verder blanco voorgeschiedenis, geen medicatie gebruik, rookt niet. -->

### Vraag 6 Wat wordt nu je werkdiagnose? 
- syncope
- CAVE: epileptisch insult niet uitgesloten


<!-- Vervolg casus
Je hebt nu tijd voor een Secondary Survey. Hierbij zie je het volgende:
    • Algemene indruk: helder en alert, niet acuut ziek
    • Hoofd/ Hals: geen wonden of hematomen
    • Thorax: vesiculair ademgeruis beiderzijds, geen wonden of hematomen
    • Cardiaal: S1S2, geen souffle
    • Abdomen: normale peristaltiek, wisselende tympanie, soepel, niet drukpijnlijk.
    • Extremiteiten: perifere pulsaties goed palpabel. Geen wonden of hematomen. Geen ossale drukpijn, geen bewegingsbeperking.  -->

### Vraag 7 Wat voor beleid stel je voor? En waarom?
- ontslag met geruststselling
- advies eten, drijkne, rustig aan doen


<!-- Casus 2
Een vrouw van 42 jaar met blanco voorgeschiedenis wordt door de ambulance naar de Spoedeisende Hulp gebracht. Vanwege de drukte vang jij de patiënte alleen op. De ambulanceverpleegkundige vertelt dat patiënte deze avond twee keer kort achter elkaar een collaps heeft gehad. Thuis troffen zij een bleke en zwetende patiënte aan. Zij kon zelf goed vertellen wat er aan de hand was en is zelf meegelopen naar de ambulance. De bloeddruk was 132/78 met een regulaire pols van 86/min. De ademhalingsfrequentie was 16/min met een saturatie van 95%. Geen aanwijzingen voor een trauma. -->

### Vraag 7 Noteer volgens ABCDE-benadering welke gegevens je hebt 
- airway; vrij?  Aanwijzingen C-spine?
- breathing: ahf 16/min, SaO2 95%
- C 123/78mmHG, HF 86/min, regulair
- D ….
- E


### Vraag 8 Welke gegevens zou je in de ABCDE nog meer willen weten? 
- A
- B
- C: ECG

<!-- Vervolg casus
Patiënte wordt overgeschoven van brancard naar het ziekenhuisbed en wordt aangesloten op de monitor. Vanaf dat moment lijkt ze plotseling niet meer te reageren. -->

### Vraag 9 Wat is het eerste dat je nu gaat doen? 
- herbeoordeling abcde
- intuberen
- O2-suppletie
- vochtsuppletie, indien geen aanwijzing voor AAA
- evt. noradrenaline (ephedrine)

<!-- Vervolg casus
Je vindt de volgende gegevens: 
A: patiënte snurkt
B: VAG bdz, geen bijgeluid, SaO2 87%, 
C: 90/60mmHg, pols 110/min, abdomen soepel, geen pulserende massa
D: E2M3V2, PEARL, voetzoolreflex bdz plantair. 
E: temp 37.3, glucose 6.3 -->

De SEH-arts komt de kamer binnen. Hij vraagt wat er aan de hand is. Ondertussen neemt de verpleegkundige bloed af. 	

### Vraag 10 Hoe zou jij dit kort en bondig overdragen?
- pt. Binnengekomen met thuis 2x collaps
- eenmaal hier, helder en aanspreekbaar
-  verloor net ineens bwz; begon te snurken
- 
- B hiertoe geintubeerd + O2 suppletie
- C geen puls massa in abdomen
- EMV score laag
- D/E niet afw.

- hiertoe ECG aangesloten met verdenking cardiale/hypovolaemische oorzaak

Vervolg casus
De patiënte blijft deze lage EMV houden. 

### Vraag 11 Wat is nu het eerste dat je zelf kan doen?

- chin lift, noodbel, call for help → start intuberen

### Vraag 12 Wat voor aanvullend onderzoek zou je bij deze patiënte willen? 

- echo abdomen: bloed in buikholte? Of retroperineaal? Aanwijzingen voor aneurysme

<!-- Vervolg casus
De SEH-arts schaalt de zorg op. De patiënte heeft een lage EMV met bedreigde ademweg, er worden voorbereidingen getroffen om de patiënte te intuberen. In de tussentijd overleggen de SEH-arts, intensivist en anesthesioloog over een plan van aanpak. Op dat moment voel je nogmaals aan de pols van patiënte, deze kan je niet meer voelen. Patiënte is in een reanimatie setting terecht gekomen.  -->

### Vraag 13 Stel een korte differentiaaldiagnose en werkdiagnose op voor deze nieuwe situatie. 

Denk hierbij aan de 4xH en 4xT.:

- Hypoxie
- Hypo/hyperK
- Hyper/hypotherm
- Hypovolaemie?
- Tension pneumoThx
- Tamponade ( --> gestuwde halsvenen, 'muffled heart tones', echo FAST/thorax)
- Thrombosis ( --> in casu longembolie: occlusie A. pulmonalis -> stuwing -> dilatatie rechterventrikel)
  + CAVE: herhaaldelijke wegraking kan passen door longembolie
- Toxins

<!-- Vervolg casus
Patiënt is inmiddels aangesloten aan de crashcar. Hierbij zie je onderstaand ritme.  -->


### Vraag 14 Welk protocol zou je nu moeten volgen als je de advanced life support (ALS) volgt? 


### Vraag 15 Bedenk voor alle van de 4xH en 4xT of je ze in deze spoedsituatie kan onderzoeken. Zo ja, hoe?

<!-- Vervolg casus
Na 3 blokken reanimeren krijgt patiënte weer sinusritme en output. De bloeduitslagen van de eerste afname komen binnen: Hb is 7.5mmol/L, Leukocyten 14.0 109/L, Natrium 134mmol/L, kalium is 4.5mmol/L. Arteriële bloedgas: pH 7.49, pCO2 4.1 kPa, PaO2 7.7kPa, HCO3 24.3mmol/L, SaO2 lactaat 2.5mmol/L. Echo cor: Rechtsbelasting, geen tamponade.  -->

### Vraag 16 wat is nu je waarschijnlijkheidsdiagnose?  Wat is je beleid?

<!-- Casus 3
In de ochtend komt een 82-jarige patiënt met zijn zoon aan op de SEH (zelfverwijzer). Hij komt met een klein wondje aan zijn rechterhand. Als u aan patiënt vraagt wat er gebeurd is geeft hij aan gevallen te zijn. Hij is niet lang buiten bewustzijn geweest. Voordat hij viel was er eigenlijk niets aan de hand. Patiënt had geen pijnklachten en was niet benauwd. -->

### Vraag 17 Wat wilt u op dit moment weten over de val om een snelle inschatting te kunnen maken van de ernst? 

<!-- Vervolg casus
Als je doorvraagt geeft patiënt aan dat hij wilde opstaan en denkt dat gestruikeld te zijn over de vloermat, hierbij heeft hij met zijn hand een deurklink geraakt. Hij is niet op zijn hoofd gevallen, denkt hij. Hij geeft aan dat het heel snel ging dus dat hij niet precies weet wat er gebeurd is. -->

### Vraag 18 Stel een korte differentiaaldiagnose op van het vallen. 

<!-- Vervolg casus
Zijn zoon vult aan: vader valt de laatste tijd regelmatig. De zoon was er dit keer ook bij en zag het gebeuren. Voordat je verder gaat met de anamnese wil je eerst een primary survey verrichten bij patiënt.  Je vindt het volgende: patiënt is goed wakker en alert, zijn glucose is 8.7, hij ademt rustig, heeft niets in zijn mond behalve een gebit dat goed zit. Hij heeft een saturatie van 93%, met ademgeruis beiderzijds met een iets verlengd expirium. Er is geen compressiepijn over de thorax, buik en bekken zijn stabiel.  Zijn polsslag is 90/min irregulair, en tensie 132/78, hij heeft geen pijn in de nek, temperatuur: 36.3C -->

### Vraag 19 Schrijf de gegevens op in ABCDE-systematiek

### Vraag 20 Wat zijn in de ABCDE je activerende gegevens? 


### Vraag 21 Wat wil je van de zoon/patiënt weten?

<!-- Vervolg casus
Je verricht een secondary survey en komt tot de volgende bevindingen: 
    • Algemene indruk: helder en alert, niet acuut ziek, vrolijke coöperatieve man 
    • Hoofd/ Hals: geen wonden of hematomen
    • Thorax: VAG beiderzijds, met een licht piepend verlengd expirium. Patiënt vertelt wel puffers te hebben voor de COPD maar deze nu niet nodig te hebben. 
    • Cardiaal: S1S2, souffle op de 2e intercostaal ruimte rechts; al langer bekend voor patiënt. 
    • Abdomen: normale peristaltiek, wisselende tympanie, soepel, niet drukpijnlijk.
    • Extremiteiten: perifere pulsaties goed palpabel. Wondje rechterhand thv MCP 1 met wijkende maar vitale wondranden, geen ossale drukpijn, geen bewegingsbeperking. Overige extremiteiten: geen wonden of hematomen.  -->

### Vraag 22 Wat wil je voor aanvullend onderzoek? 

<!-- Vervolg casus
De zoon van patiënt vertelt dat het de laatste tijd eigenlijk weer net iets beter ging met patiënt. Hij is een week geleden bij de huisarts geweest omdat hij veel vocht vasthield. Nu de huisarts plastabletten heeft voorgeschreven is het vocht duidelijk afgenomen.  -->

### Vraag 23 Wat wordt nu je werkdiagnose? Hoe zou je deze bevestigen? 

### Vraag 24 Wat zijn je vervolgstappen in de behandeling en zorg voor patiënt? 
