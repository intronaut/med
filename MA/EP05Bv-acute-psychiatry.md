## EP05Bv-acute-psychiatry

<!-- Achtergrond
Veel patiënten met acute psychiatrische problemen komen terecht bij de crisisdienst, met name als het een ontsporing van een bekend psychiatrisch probleem betreft. Patiënten met psychiatrische problemen worden ook frequent op de spoedeisende hulp gezien. Zij kunnen bijvoorbeeld wonden hebben die behandeld moeten worden, suf zijn bij een auto-intoxicatie in het kader van een tentamen suïcide of een organisch substraat als mogelijke oorzaak van de psychische ontregeling moet onderzocht worden. Grensoverschrijdend en agressief gedrag komt frequent voor op de SEH. Vaak liggen alcohol of drugsgebruik, frustratie, angst of psychische aandoeningen hieraan ten grondslag. Dit gedrag varieert van verbale agressie ‘ik zit hier nu al een uur te wachten, als ik niet snel geholpen word dan…’ tot (poging tot) fysieke agressie.
In deze ZSO gaan we in op verwardheid, agitatie / agressie, tentamen suïcide en de Wvggz, Wet verplichte geestelijke gezondheidszorg en de WZD, Wet zorg en dwang	-->


### Literatuur te bestuderen voorafgaand aan ZSO:
    • Leerboek Acute geneeskunde, hoofdstuk verwardheid
    • Zakkaartje psychiatrie
    • https://www.ypsilon.org/psychose
    • https://www.vergiftigingen.info
    • http://handreiking.ysis.nl/wilsbekwaamheid/criteria-van-wilsbekwaamheid
    • https://wetten.overheid.nl/BWBR0040635/2022-01-01
    • https://wetten.overheid.nl/BWBR0040632/2021-11-06/0

<!-- Casus 1
Je bent werkzaam op de spoedeisende hulp. De triage verpleegkundige zoekt je op. Er is net een melding binnengekomen van de politie. Zij zijn binnen 5 minuten op de SEH met een verwarde man, geschatte leeftijd rond de 70 jaar. Patiënt liep bloedend over straat, omstanders hebben de politie gebeld. Vanwege de wonden heeft de politie de crisisdienst nog niet ingeschakeld en komen ze naar de spoedeisende hulp. De SEH-arts staat vast bij een trauma, dus de verpleegkundige vraagt jou deze patiënt op de vangen.  -->

### Vraag 1 Welke gegevens zou je graag willen hebben voordat de patiënt binnengebracht wordt?
- naam, bsn, evt. contactpersoon
- VG
- 

### Vraag 2 Schakel je hulp in? Wat kun je doen om je voor te bereiden op deze patiënt?
- vpk vragen
- 

### Vraag 3 In je hoofd ben je alvast bezig een DD te maken op basis van dit verhaal. Wat kan
de oorzaak zijn van de verwardheid?
- intox (alcohol, opiaten, auto-intox)
- psychiatrisch (delier, psychose, manie)
- hoofd-hersenletsel
- RIP in cerebro / bloeding in cerebro
- dehydratie
- bijwerking medicatie

<!-- Vervolg casus
De politie komt binnen met de patiënt, meneer Jansen van 68 jaar. Zij hebben weinig nieuwe informatie voor je. Meneer Jansen reageerde wat geïrriteerd op de omstanders en politie, maar is niet agressief geweest. Er is een zoon gelokaliseerd, deze is gebeld en komt naar de SEH. De politie vertrekt weer. Je vraagt de verpleegkundige de controles te verrichten en ondertussen start je met de anamnese. Meneer Jansen weet niet meer wat hij op straat deed. Als je vraagt naar zijn wonden vertelt hij dat hij gevallen is. -->

### Vraag 4 Wat wil je nog meer van patiënt weten met de eerder gemaakte DD in je
achterhoofd?
- eerder dergelijke situatie meegemaakt? Bekend met neurologisiche of psychiatrische problematiek?
- hoe bent u gevallen / waardoor bent u gevallen?
- gebruikt u medicatie? (psycofarmaca, antihhypertensiva, antidiabetica)
- heeft u alcohol / drugs gebruikt?

### Vraag 5 Waar let je specifiek op tijdens het psychiatrische onderzoek?
- verandering bwz ; wisselend? Continue? Herseld?
- 


<!-- Vervolg casus
De verpleegkundige heeft inmiddels de controles gedaan. SpO2 95% zonder zuurstof,
ademfrequentie 16/min, RR 140/85 mmHg, pols 102/min, T 37.3­C. Je schat de anamnese
niet betrouwbaar in, er is namelijk een discrepantie tussen jouw vraag en het antwoord van
meneer Jansen. Bovendien kan meneer Jansen zijn aandacht niet bij het gesprek houden.
Inmiddels weet je wel dat meneer Jansen niet georiënteerd is in tijd en persoon. Hij weet wel
dat hij in het ziekenhuis is, maar kan de afdeling niet noemen. Meneer Jansen vertelt je van
alles, maar er zit geen samenhang in zijn verhaal. Je gaat verder met het lichamelijk
onderzoek.
Je ziet een slecht verzorgde, magere man. Hij ruikt naar urine. Er is geen alcohol foetor.
A: vrij, CWK niet drukpijnlijk (echter niet betrouwbaar)
B: symmetrische thoraxexcursies, VAG met bibasaal enkele fijne crepitaties
C: NP, WT, diffuus drukgevoelige buik, geen actief spierverzet, lever en milt niet palpabel
D: EMV maximaal (echter wel verward), pupillen PEARL, geen lateralisatie
E: wond van 3cm lateraal op linker bovenbeen, schrammen op benen en handen. Geen
zwelling of excoriaties in aangezicht of behaarde hoofd.
Verder valt het je op dat meneer Jansen frequent langs je kijkt en weinig oogcontact maakt.
Zijn vingers zijn continu in beweging, het lijkt alsof hij pluisjes van zijn trui plukt. Tijdens het consult staat hij ook ineens op en gaat door de kamer wandelen. -->

### Vraag 6 Wil je aanvullend onderzoek inzetten? Zo ja, welke onderzoeken?
- glucose, electrolyten, toxlab, crp/bse + Hb/Ht


### Vraag 7 Welke factoren pleiten voor / tegen een acute psychose?
- pro: incoherentie, tangentialiteit. Weinig oogcontact, frequent omkijken. Pyschomotore agitatie, abberant gedrag. Concentratiestoornis
- contra: mogelijkheid delier ; 


<!-- Vervolg casus
De zoon van meneer Jansen komt binnen. Je neemt een heteroanamnese af. Patiënt is sinds
een jaar weduwnaar. Hij woont zelfstandig en heeft geen thuiszorg. Zoon maakt zich zorgen
om zijn vader, omdat hij na het overlijden van zijn echtgenote zich heeft teruggetrokken en
meer alcohol drinkt. Er is geen psychiatrische voorgeschiedenis, geen dementie. Het is
onbekend of patiënt recent koorts heeft gehad. Wel heeft hij een paar dagen geleden aan zijn
zoon verteld dat hij wat misselijk was en buikpijn had. Hij was toen niet verward. -->

### Vraag 8 Wat is op dit moment je werkdiagnose?
- delier cq alcoholontrekkingsdelier

### Vraag 9 Welke behandeling ga je inzetten? Neem je patiënt op?
- wonden hechten
- opnemen psychiatrie
- behandelen delier ; corrigeren electrolyten, 
- diazepam/lorazepam

### Vraag 10 Vind je deze patiënt wilsbekwaam? Waarom wel / niet?

<!-- Casus 2
Het is zaterdagnacht. Een 26- jarige man is in de stad opgepakt bij een vechtpartij en wordt door de ambulance en politie naar de spoedeisende  gebracht. Hij is onrustig en geagiteerd. Ze zijn er over 10 minuten. -->

### Vraag 11 Hoe herken je grensoverschrijdend gedrag?
- 

### Vraag 12 Je bereidt je voor op deze patiënt, die mogelijk agressief kan worden. Wat kun je doen om een geagiteerde / agressieve patiënt te kalmeren?
- deescaleren door praten
- prikkelarme omgeving
- uitleg:  wij zijn om te helpen, en u heeft de hulp nodig,  maar op deze manier is dat niet mogelijk..
- platspuiten
- evt contactpersonen/naasten betrekken

<!-- Vervolg casus
De overdracht van de ambulance is kort. Patiënt heeft verteld dat hij werd achtervolgd door politie in burger. Hij heeft deze persoon hierop aangesproken en dit is uitgemond in een vechtpartij. Patiënt heeft bebloede knokkels en een zwelling van zijn rechteroog. Hij ruikt naar alcohol. Verder hebben zij hem niet na kunnen kijken. Controles zijn niet gedaan. -->

### Vraag 13: Wat is op dit moment je DD en werkdiagnose?
- paranoide waan  (waanstoornis)

### Vraag 14: Welk aanvullend onderzoek wil je inzetten?
- lab: toxlab, icc 

<!-- Vervolg casus
Bij het afstappen van de ambulance brancard valt patiënt de ambulance broeder aan. Hij
probeert hem te slaan. De politie duikt er direct op. Met 4 man is het lastig patiënt om hem in
bedwang te houden. De verpleegkundige roept naar jou: ‘dokter, doe iets!’ -->

Vraag 15 Wat doe je?
- noodbel indrukken
- Benzo’s / Ketamine IM


<!-- Casus 3
Een 42-jarige vrouw wordt door de ambulance naar de spoedeisende hulp gebracht. Zij door
haar echtgenoot verminderd aanspreekbaar thuis op bed aangetroffen. Een uur eerder had zij
zijn voicemail ingesproken en verteld dat zij een einde aan haar leven ging maken. Bij
aankomst van de ambulance heeft zij een EMV score van E2M5V2. Er worden lege strips
tabletten in de prullenbak gevonden. Patiënte heeft mogelijk 30x oxazepam 10mg ingenomen. -->

### Vraag 16 De verwachte inname tijd is ongeveer 1 uur geleden. Wat zijn nu de therapeutische mogelijkheden? Beschrijf uw overwegingen.

- flumazenil, maar alleen bij indien ademdepressie

### Vraag 17 Wie vraag je in consult?

### Vraag 18: Neem je deze patiënte op? Zo ja, op welke afdeling?

Wvggz / WZD

### Vraag 19 Is deze patiënt wilsbekwaam? Wie bepaalt dat? En hoe?

### Vraag 20 Kun je in Nederland patiënten gedwongen behandelen?



