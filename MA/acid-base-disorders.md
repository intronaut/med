## Zuur-base stoornis en bloedgasanalyse

### arterieel bloedgas

- pH
- PaCO2
- HCO3- en BE
- PaO2

### normaalwaarden / reference values

- pH(n) = 7.35-7.45
- [H+]n: 36-44 nmol/l
- acidaemie/acidose: pH<7.35
- alkalaemie/alkalose: pH>7.45
- stricte homeostase van pH in het lichaam is mogelijk door aanwezigheid van buffers:
  + intracellulair: proteins, fosfaat, Hb
  + extracellulair: plasma protein, HCO3
- dus: pas na uitputting van de buffers, kan de pH buiten de normaalwaarde treden.

### Gas exchange / gaswisseling

- bij 1atm is de kPa in een mengsel van gas numeriek gelijk aan percentage, i.e.:  @1atm: 21% O2 == paO2=21 kPa
- `CO2 + H20 <->H+ + HCO3-`
- PaCO2(n)=4.7-6.0 kPa (35-45mmHg)
- HCO3-(n)=22-26 mmol/l

**concluderend**  
- daling van de **alveolaire ventilatie** zal de uitscheiding van CO2 verminderen m.a.g. toenamae PaCo2 -> daling pH -> ontstaan resp acidose
- toename van de **alveolaire ventilatie** zal de uitscheiding van CO2 verhogen m.a.g. afname PaCo2 -> stijging pH -> ontstaan resp alkalose
- Ad. primaire compensatie pH is respiratoir dmv PaCO2 (afblazen zuurte)

### Bicarbonaat en BE

**basics**  
- 90% van overtollig zuur blaas je uit
- 10% metabool via de urine

**definities**  
- `CO2 + H2CO3- <-> H+ + H2CO3-`
- BE(n) = -2 -- +2 mmol/l
  + BE<-2 -> metab acid
  + BE>+2 -> metab alka 
- Base Excess = maat van overtollig zuur/base t.g.v. metabolische ontregeling
- 'Base deficit' wordt helaas ook 'negative base excess' genoemd.


- metabole buffering middels HCO3- gesecreteerd door de nieren duurt uren-dagen
- H2CO3 heet ook wel 'koolzuur'
- Koolzuuranhydrase is het enzym dat zuur-base-homeostase met H+ katalyseert
- `CO2 + HCO3- <-> H2CO3 <-> H+ + HCO3-`

**misc**  
- In de lucht zijn de normale pO2 en PCO2 hoger dan die in het bloed, omdat gasuitwisseling natuurlijk niet 100% effectief is.
- approx. zijn de waarde 10kPa lager in het bloed, dan in de lucht.
- NOTE:  PaO2  daalt met toenemende leeftijd tot ongeveer 10kPa bij 75yo en neemt dan weer toe tot 11kPa op 85yo.

## stappenplan interpreteren zuur-base analyse

- 1) Klinische presentatie (en VG) vd patient en beoordeling klinische ernst.
  + de presentatie kan daarnaast richting geven aan het vinden vd onderliggende oorzaak.
- 2) Hypoxaemie? normaliter op kamerlucht PaCO2 = 10.0-13.0 ; echter CAVE: O2-suppletie in acht nemen in acht nemen bij interpretatie van deze waarden. 
- 3) pH<7.35 OF pH>7.45 : Acidaemie of alkalaemie?
  + CAVE: pH binnen de normaalwaardes suggereren chronische toestand
- 4) PaCO2 beoordelen
  + 4a. bij pH<7.35 en PaCO2 >6.0kPa -> sprake van acicdose, die gedeeltelijk of volledig de ontregeling verklaard
  + 4b. bij pH>7.45 en PaCO2 <4.7 -> sprake van resp alkalose, komt geisoleerd nauwelijsk voor. Wordt met name gezien bij mechanisch ventileren met te hoge frequenties of teugvolumes -> PaCO2 daalt -> H+ daalt -> ontstaan alkalose
- 5. HCO3- en BE beoordelen
  + bij pH<7.35 en BE< -2mmol / HCO3- < 22mmol/l -> sprake van metabole acidose, die gedeetelijkjof volledig ontregeling verklaart. Indien PaCO2 ook is verhoogd: sprake van respiratoire component.
  + bij pH>7.45 en BE> + 2mmol/l / HCO3- > 26mmol -> sprake van metabole alkalose. CAVE: Indien PaCO2 ook is verlaagd: sprake van respiratoire component.


### Gecompenseerde zuur-base stoornis interpretatie

| process               | pH        | CO2   | BE/HCO3-   | compensation |
|-----------------------|-----------|-------|------------|--------------|
| metabolic acidosis	  | <7.40     | <4.7	|  <-2 / <22 | respiratory  |
| respiratory acidosis	| <7.40 	  | >6.0	| >+2 / >26  | renal        |
| metabolic alkalosis	  | >7.40	    | >6.0	|  >+2 / >26 | respiratory  |
| respiratory alkalosis	| >7.40   	| <4.7 	| <-2 / <22  | renal        |

