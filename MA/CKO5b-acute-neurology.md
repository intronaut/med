## CKO5Bv-acute-neurology

### Vraag 1 Wat wil je bij binnenkomst op de SEH nog meer weten van de ambulanceverpleegkundige? 

- waren er omstanders?
- therapietrouw medicamenteus?
- intoxicaties?
- provocatie / 
- trauma?

### Vraag 2 Waarmee start je als de ambulance de volledige overdracht heeft gedaan?
- acuut inschatten mogelijk hoofd-hersenletsel
- screenend NO ; met focus op DD infarct
- CT-

<!-- Vervolg casus
Tijdens het lichamelijk onderzoek op de SEH krijgt patiënt opnieuw een gegeneraliseerd tonisch-clonisch insult (trekkingen van armen en benen). Daarbij loopt zijn gelaat blauw aan en komt er een klein spoortje bloed uit de mond.  -->

### Vraag 3 Wat is het eerste wat je op dit moment zult doen? 
- midazolam 2mg/spray acuut zn ; herhalen indien insult >3min
- 

## Vraag 4 Hoelang moet een insult duren voor we van een status epilepticus spreken (conform de laatste richtlijn)?
-

### Vraag 5 Wat is het gevaar van een te lang durend insult?
- hypoctisch hersenschade

### Vraag 6 Als de patiënt blijkt alcoholabusus te hebben in zijn medische voorgeschiedenis, wat zou je dan nog kunnen toevoegen aan de behandeling? 
- vit B complex IM / vit B1 IM
- vit C suppletie

<!-- Vervolg casus; Patiënt komt na 2 maal rivotril 0,5mg iv uit zijn insult. Daarbij heeft de patiënt wederom een laterale tongbeet opgelopen, echter dit bloed nu niet hard en zijn ademweg is niet bedreigd. Patient heeft wederom een fors verlaagd bewustzijn met een EMV-score van E-2, M-4, V-3. -->


### Vraag 7 Wat is de meest waarschijnlijke reden van het fors verlaagde bewustzijn? Moet patiënt nu geïntubeerd worden? 

- post-ictaal neurologisch
- relat hypoxaemie in cerebro
- nee, geen intuberen

### Vraag 8 Als een patiënt zoals deze binnenkomt met een wegraking met laterale tongbeet, urine-incontinentie en post-ictale verwardheid als enige verschijnselen, wat is dan de meest specifieke afwijking voor een insult als oorzaak van de wegraking? 

- couperen dmv benzo's? →
- epileptisch insult

### Vraag 9 Hoe wordt de motorische neurologische uitval genoemd die na een insult kan optreden? 

- Toddse parese


<!--Casus 2
Je wordt door de SEH-arts gestuurd naar een vrouw van 78 jaar die zojuist door haar dochter op de spoedeisende hulp wordt binnengebracht (als zelfverwijzer) omdat ze haar moeder ‘raar’ vond praten en dit totaal niet vertrouwde. Dit was een half uur geleden voor het eerst opgevallen toen ze bij haar moeder thuis op bezoek kwam. Verder zijn er geen andere klachten en zit patiente helder rechtop in een stoel wanneer je bij haar binnenkomt. Voorgeschiedenis vermeld een hartinfarct, hypertensie en diabetes mellitus type 2. -->

### Vraag 10 Wat wil je graag nog meer weten op het moment dat je binnenkomt bij deze patiënt? 

- glucose? Zelf opgemeten vandaag?  Medicatie ingenomen?
- wat was er raar aan de spraak?
- waarom vertrouwde dochter het niet?
- last seen well

<!-- Vervolg casus; Bij navraag blijkt de patiënt erg veel moeite te hebben met op woorden komen in het gesprek, maar begrijpt erg goed wat je aan haar vraagt (woordvindstoornissen). Daarnaast vertelt de dochter dat ze haar een uur geleden nog had gebeld of het uit kwam dat ze op bezoek kwam. Op dat moment sprak ze vlot en had totaal geen moeite met zoeken van woorden. -->
 
### Vraag 11 Wat is, gezien deze extra informatie, je eerst volgende stap? En waarom? 

- CT-c blanco
- CTa

### Vraag 12 Wat is het tijdswindow waarin trombolyse kan plaatsvinden? 

- overweging thrombolyse window (4,5h vs. Tot 6h)


### Vraag 13 Wat zijn de relatieve- en absolute contraindicaties voor trombolyse volgens het protocol? 

- maligne hypertensie
- hypertensie >185/>110mmHg
- relatieve :  antithrombotica/bloedverdunners

### Contra-indicaties

- intracraniële bloeding (inclusief hemorragische transformatie infarct) bij beeldvorming;
- matig/ernstig schedelhersenletsel in voorgaande twee maanden;
- herseninfarct in voorgaande twee maanden;
- intracraniële bloeding in voorgaande drie maanden;
- bloeding in maag-darmkanaal of urinewegen in voorgaande twee weken;
- grote chirurgische ingreep in voorgaande twee wekenb;
- bloeddruk systolisch ≥ 185 mmHg of diastolisch ≥ 110 mmHgc;
- actieve bloeding of traumatisch letsel (bijvoorbeeld fractuur);
- gebruik van vitamine K-antagonist en INR > 1,7;
- gebruik directe trombine- of factor Xa-remmer (direct oraal anticoagulantium; DOAC) - zie ook hieronder)d;
- gebruik LMW-heparine in therapeutische dosering, tenzij meer dan 4 uur geleden een - subcutane gift en anti Xa-spiegels normaale;
- trombocyten < 100 x 109/L (was < 90 x 109/L;
- serum glucose < 2,7 mmol/L als mogelijke oorzaak van de verschijnselen, of serum glucose >22,0 mmol/L;

### Vraag 14 Wat is in geval van hypertensie (systolisch > 185 en/of diastolisch >110) de eerste keus antihypertensivum bij trombolysekandidaten? 

- labetalol
- nicardipine

<!-- Casus 3
Via de ambulance wordt een man van 35 binnengebracht met peracuut hevige hoofdpijnklachten gegeneraliseerd met uitstraling naar de nek, hij is daarbij suf. De voorgeschiedenis is blanco en patiënt gebruikt geen medicatie. -->

### Vraag 15 Wat doe je als eerste bij de opvang van deze patiënt? 

- abcde
- 


### Vraag 16 Wat wil je in de anamnese nog meer weten van deze patiënt (of diens begeleider)? 
- zieken omgeving
- vaccinatie
- koorts gehad


### Vraag 17 Wat is je differentiaal diagnose van deze patiënt met hoofdpijnklachten op dit moment? 
- virale meningitis indien nekstijf
- thunderclap / SAB
- migraine

### Vraag 18 Wat is je volgende stap? 
- 


### Vraag 19 Als deze patiënt 8 uur na ontstaan van klachten was gekomen, en de CT cerebrum liet geen bloeding zien, zou er dan met zekerheid geen sprake zijn van een SAB? Wat zijn de afkaptijden voor diagnostiek? 
- NEE, kunnen ook langzaam bloeden
- indien CTc g b;  LP + liquoronderzoek pas >12h na ontstaan hoofdpijn

### Vraag 20 Wat zou een andere mogelijkheid kunnen zijn om een SAB aan te tonen of uit te sluiten? 

- aan/afwezigheid bilirubine in liquor bij LP