---
title: pubmed-not
author: intronaut
created: 2023-06-28
updated: 2023-06-28
version: 0.01a
---

## pubmed-not

### helpful links (uni specific)

- [ru-medlib-sources](https://libguides.ru.nl/medisch/bronnen)
- [pubmed-ru](https://libguides.ru.nl/go.php?c=5567722) 
- [MeSH-home](https://www.nlm.nih.gov/mesh/meshhome.html)
- [MeSH-search](https://meshb.nlm.nih.gov/search)
- [the-MeSH-machine](https://meshb.nlm.nih.gov/MeSHonDemand) -- tool for generation of most similir MeSH terms from your text input (i.e. **MeSH-magicwand**)
- [MeSH-search-advanced](https://www.ncbi.nlm.nih.gov/mesh/advanced/)
- [pubmed-search-builder](https://pubmed.ncbi.nlm.nih.gov/advanced/)
- [pubmed-search-doc](https://www.ncbi.nlm.nih.gov/books/NBK3837/#EntrezHelp.Using_the_Advanced_Search_Pag) -- **pubMed search bible**, all there is to know
- [MeSH-search-autocomplete](https://id.nlm.nih.gov/mesh/lookup?form=descriptor&label=health&year=current)
- [yt-pubmed-mesh-search](https://www.youtube.com/watch?v=uyF8uQY9wys)
- ~~[login-to-supersearcher](https://auth.nih.gov/CertAuthV3/forms/researchorgs.aspx?TARGET=-SM-https://login--prod.nlm.nih.gov/uts/login?service=https://uts.nlm.nih.gov/uts/auth)  (search for `Radboud Universiteit` OR `Radboudumc` to get RU(mc) login)~~ de RU zelf geeft ons geen licence en het Radboudumc heeft standaard bepaalt dat de supersearcher niet iets is waar je zomaar toegang tot hebt.. Actually uberhaupt niet ; je moet aantonen dat je hem nodig hebt -- darndest


- - - -

## PICO

Moeten we alle zwangeren >35jr een inleiding vd baring aanbieden?

| . | desc |
|---|------|
| P | >35yo, F, pregnant |
| I | inleiding partus |
| C | expectatief beleid |
| O | perinatale morbiditeit en mortaliteit. | 

- secondary? maternal morbidity/mortality







