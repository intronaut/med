## ZSO buikpijn

### Casus 1

- aard/locatie pijn
- pijn ter hoogte van punt McBurney
- defense musculaire
- psoas sign
- tekenen peritoneale prikkelikng
- koorts, braken
- defaecatie

Vraag 2

- bloed/slijmbijmening?
- kleur braaksel? hoeveelheid braaksel? zieken in omgeving?
- eerder buik-OK?
- AO beeld:  echo abdomen, focus appendix  (evt. CXT-abdomen)
- AO lab:  CRP, leukodiff, bloedbeeld, defkweken, electrolyten

### Behandeling

- Amox/clav 4dd1200mg IV
- monitoring
- indien verergering -> heroverwegen OK

### Casus 2

- cave AAA

- vloeistof suppletie
- CT-scan
- naar OK

- 6.5cm is groter dan acceptabel (= 5.5cm max, vrouwen 5cm max)

- Tx:
  + EVAR
  + open chirrugie: Ao-buis / Ao-bi-iliacaal / Ao-bifem prothesse

- overleving AAA, afh locatie laesie
  + retroperitoneaal = te overleven
  + intra abdominaal = 120sec resterend, max

Vraag 15: we hebben wel ideen, maar wat moeten we precies

### Casus 2

- Anamnese
  + wat is er anders dat u nu komt?
  + sprake van geweld / misbruik
- LO
  + toegebracht trauma / tekenen misbruik
  + geprikkelde buik
 
- Lab
  + CRP +leukodiff, BSE
  + hCG
  - Hb, Ht
- verwijzing gyn, indien cyclusgebonden klachten

- DD langdurige buik
  + 
