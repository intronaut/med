## Table of Contents

- A. Physiology
    + A1 -- Risk Factors
    + A2 -- Chest pain
    + A3 -- Dyspnoea and respiratory problems
    + A4 -- Cough and hemoptoe
    + A5 -- Palpitations
    + A6 -- Renal damage
    + A7 -- Oedema
- B. Nervous system and Movement
    + B1 -- Nervous System 1a (motor)
    + B2 -- Eyes and visus
    + B3 -- Ears and hearing
    + B4 -- Nervous System 1b (sensor)
    + B5 -- Orthopaedics and Kinesiology
- C. Internal organs
    + C1 -- Gastroenterology A
    + C2 -- Gastroenterologie B
    + C3 -- Urogenital problems in the fertile phase of life
    + C4 -- Pregnancy
    + C5 -- Benigh urogynaecology and proctology
    + C6 -- Specialist oncology
    + C7 -- Acute abdominal pain and infection
- D. Psychiatry and Dermatology
    + D1 -- Headache and anxiety
    + D2 -- Changes in consciousness
    + D3 -- Psychiatry A
    + D4 -- Pyschiatry B
    + D5 -- Dermatology
- E. General Medicine
    + E1 -- Regular complaints, infectious disease, edocrinological disease
    + E2 -- Acute complex healthcare
    + E3 -- Oncology, palliative treament and end-of-life
    + E4 -- Chronic illness and specific patient subpopulations
    + E5 -- Clinical skills
- F. Extra content
    + [KFYSXX](https://git.disroot.org/intronaut/KFYSXX.git) -- Food & Health ('Voeding & Gezondheid A-B-C-D')
