---
author: intronaut  
created: 2020-06-11  
updated: 2023-06-27  
description: Medicine - collected notes  
license: CC4.0 | BY-SA  
keywords: med medicine university not notes  
---

## med

- [Bachelor-syllabus-toc](./BA/toc.md)
- [pubmed](./MA/pubmed.md)